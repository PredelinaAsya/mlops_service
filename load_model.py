import mlflow


client = mlflow.MlflowClient("http://94.228.113.199:5000")
model_info = client.get_latest_versions('best_model.pt')[0]
mlflow.artifacts.download_artifacts(artifact_uri=model_info.source, dst_path='models')
